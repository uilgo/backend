package org.iesba.uilgo.util;

import java.math.BigDecimal;

import org.iesba.uilgo.model.Subscription;

import com.stripe.Stripe;
import com.stripe.model.checkout.Session;
import com.stripe.param.checkout.SessionCreateParams;

public class StripePaymentUtil {

	public String[] checkout(Subscription subscription) throws Exception {

		Stripe.apiKey = "sk_test_Sk7ACr6uAhnfTyRlSMPme5lb00Barxax9T";

		BigDecimal valueAmountCents = subscription.getTicket().getPrice().multiply(new BigDecimal("100"));
		
		SessionCreateParams params = SessionCreateParams.builder()
				.addPaymentMethodType(SessionCreateParams.PaymentMethodType.CARD)
				.addPaymentMethodType(SessionCreateParams.PaymentMethodType.CARD)
				.addLineItem(SessionCreateParams.LineItem.builder().setName(subscription.getTicket().getDescription())
						.setDescription(subscription.getTicket().getDescription())
						.setAmount(valueAmountCents.longValueExact())
						.setQuantity(1L).setCurrency("BRL").build())
				.setSuccessUrl(
						"http://prod-uilgo.jelastic.saveincloud.net/successpage?session_id={CHECKOUT_SESSION_ID}")
				.setCancelUrl("http://front-uilgo.jelastic.saveincloud.net/pagamento-cancelado").build();

		Session session = Session.create(params);

		return new String[] { session.getId() };
	}
}
