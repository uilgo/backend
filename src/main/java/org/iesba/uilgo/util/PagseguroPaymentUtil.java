package org.iesba.uilgo.util;

import java.math.BigDecimal;

import org.iesba.uilgo.model.Subscription;

import br.com.uol.pagseguro.api.PagSeguro;
import br.com.uol.pagseguro.api.PagSeguroEnv;
import br.com.uol.pagseguro.api.checkout.CheckoutRegistrationBuilder;
import br.com.uol.pagseguro.api.checkout.RegisteredCheckout;
import br.com.uol.pagseguro.api.common.domain.builder.ConfigBuilder;
import br.com.uol.pagseguro.api.common.domain.builder.PaymentItemBuilder;
import br.com.uol.pagseguro.api.common.domain.builder.PaymentMethodBuilder;
import br.com.uol.pagseguro.api.common.domain.builder.PaymentMethodConfigBuilder;
import br.com.uol.pagseguro.api.common.domain.builder.SenderBuilder;
import br.com.uol.pagseguro.api.common.domain.enums.ConfigKey;
import br.com.uol.pagseguro.api.common.domain.enums.PaymentMethodGroup;
import br.com.uol.pagseguro.api.credential.Credential;
import br.com.uol.pagseguro.api.http.JSEHttpClient;
import br.com.uol.pagseguro.api.utils.logging.SimpleLoggerFactory;

public class PagseguroPaymentUtil {

	public String[] checkout(Subscription subscription) throws Exception {

		final PagSeguro pagSeguro = PagSeguro.instance(new SimpleLoggerFactory(), new JSEHttpClient(),
				Credential.sellerCredential("deni.mascarenhas@gmail.com", "83100DC82CE64B92ACCA1F26EEE0F351"),
				PagSeguroEnv.SANDBOX);

		CheckoutRegistrationBuilder checkoutRegistrationBuilder = new CheckoutRegistrationBuilder()
				.withSender(new SenderBuilder()
						.withEmail("uilgo@sandbox.pagseguro.com.br"/* subscription.getMember().getEmail() */))
				.addItem(new PaymentItemBuilder().withId(subscription.getId() + "")
						.withDescription(subscription.getTicket().getDescription())
						.withAmount(subscription.getTicket().getPrice()).withQuantity(1))
				.addPaymentMethodConfig(new PaymentMethodConfigBuilder()
						.withPaymentMethod(new PaymentMethodBuilder().withGroup(PaymentMethodGroup.CREDIT_CARD))
						.withConfig(new ConfigBuilder().withKey(ConfigKey.MAX_INSTALLMENTS_LIMIT)
								.withValue(new BigDecimal(10))))
				.addPaymentMethodConfig(new PaymentMethodConfigBuilder()
						.withPaymentMethod(new PaymentMethodBuilder().withGroup(PaymentMethodGroup.CREDIT_CARD))
						.withConfig(new ConfigBuilder().withKey(ConfigKey.MAX_INSTALLMENTS_NO_INTEREST)
								.withValue(new BigDecimal(5))));

		RegisteredCheckout registeredCheckout = pagSeguro.checkouts().register(checkoutRegistrationBuilder);

		System.out.println(registeredCheckout.getRedirectURL());

		return new String[] { registeredCheckout.getCheckoutCode(), registeredCheckout.getRedirectURL() };
	}

}
