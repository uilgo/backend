package org.iesba.uilgo.util;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.iesba.uilgo.rest.EventRestResource;
import org.iesba.uilgo.rest.MemberRestResource;
import org.iesba.uilgo.rest.SubscriptionRestResource;
import org.iesba.uilgo.rest.UserRestResource;

import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;

@ApplicationPath("/rest")
public class JaxRsActivator extends Application {

	public JaxRsActivator() {
		BeanConfig conf = new BeanConfig();
		conf.setTitle("Uilgo API");
		conf.setDescription("Uilgo - API responsável por controlar eventos e inscrições de membros em seus eventos.");
		conf.setVersion("1.0.0");
		//conf.setHost("localhost:8080/backend");
		conf.setHost("prod-uilgo.jelastic.saveincloud.net");
		conf.setBasePath("/rest");
		conf.setSchemes(new String[] { "http" });
		conf.setResourcePackage("org.iesba.uilgo.rest");
		conf.setScan(true);
	}

	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> resources = new HashSet<>();
		resources.add(EventRestResource.class);
		resources.add(MemberRestResource.class);
		resources.add(SubscriptionRestResource.class);
		resources.add(UserRestResource.class);
		resources.add(ApiListingResource.class);
		resources.add(SwaggerSerializers.class);
		resources.add(CORSResponseFilter.class);

		return resources;
	}

}
