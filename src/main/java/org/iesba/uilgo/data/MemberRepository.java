package org.iesba.uilgo.data;

import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.iesba.uilgo.model.Member;

@ApplicationScoped
public class MemberRepository {

	@Inject
	private EntityManager em;

	public Member findById(Long id) {
		return em.find(Member.class, id);
	}

	public Member findByEmail(String email) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Member> criteria = cb.createQuery(Member.class);
		Root<Member> member = criteria.from(Member.class);

		criteria.select(member).where(cb.like(cb.upper(member.get("email")), "%" + email.toUpperCase() + "%"));
		criteria.orderBy(cb.asc(member.get("name")));
		
		return em.createQuery(criteria).getSingleResult();
	}

	public List<Member> findAllOrderedByName() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Member> criteria = cb.createQuery(Member.class);
		Root<Member> member = criteria.from(Member.class);

		criteria.select(member).orderBy(cb.asc(member.get("name")));

		return em.createQuery(criteria).getResultList();
	}

	public List<Member> findByFilter(String query) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Member> criteria = cb.createQuery(Member.class);
		Root<Member> member = criteria.from(Member.class);

		criteria.multiselect(member.get("id"), member.get("email"), member.get("identityNumber"))
				.where(cb.or(cb.equal(cb.upper(member.<String>get("name")), query.toUpperCase()),
						cb.equal(cb.upper(member.<String>get("email")), query.toUpperCase()),
						cb.equal(member.<String>get("identityNumber"), query.toUpperCase())));
		criteria.orderBy(cb.asc(member.get("name")));

		return em.createQuery(criteria).getResultList();
	}

	public Member findByBirth(Date birthDate, Long id) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Member> criteria = cb.createQuery(Member.class);
		Root<Member> member = criteria.from(Member.class);

		criteria.multiselect(member.get("id"), member.get("birth"))
				.where(cb.and(cb.equal(member.get("birth"), birthDate), cb.equal(member.get("id"), id)));
		criteria.orderBy(cb.asc(member.get("name")));

		return em.createQuery(criteria).getSingleResult();
	}
}
