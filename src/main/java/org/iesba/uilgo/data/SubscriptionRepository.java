package org.iesba.uilgo.data;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.iesba.uilgo.model.Member;
import org.iesba.uilgo.model.Subscription;
import org.iesba.uilgo.model.Ticket;

@ApplicationScoped
public class SubscriptionRepository {

	@Inject
	private EntityManager em;

	public Subscription findById(Long id) {
		return em.find(Subscription.class, id);
	}

	public Subscription findByPaymentId(String paymentId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Subscription> criteria = cb.createQuery(Subscription.class);
		Root<Subscription> subscription = criteria.from(Subscription.class);
		criteria.select(subscription).where(cb.equal(subscription.get("paymentId"), paymentId));
		return em.createQuery(criteria).getSingleResult();
	}

	public List<Subscription> findByMemberandTicket(Member member, Ticket ticket) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Subscription> criteria = cb.createQuery(Subscription.class);
		Root<Subscription> subscription = criteria.from(Subscription.class);
		criteria.select(subscription).where(
				cb.and(cb.equal(subscription.get("member"), member), cb.equal(subscription.get("ticket"), ticket)));
		return em.createQuery(criteria).getResultList();
	}

}
