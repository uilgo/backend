package org.iesba.uilgo.rest;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import org.iesba.uilgo.data.SubscriptionRepository;
import org.iesba.uilgo.model.Member;
import org.iesba.uilgo.model.Subscription;
import org.iesba.uilgo.model.Ticket;
import org.iesba.uilgo.service.SubscriptionRegistration;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 
 */
@Api("API responsável por operações em inscrições.")
@Stateless
@Path("/subscriptions")
public class SubscriptionRestResource {

	@PersistenceContext(unitName = "primary")
	private EntityManager em;

	@Inject
	private SubscriptionRegistration subscriptionRegistration;

	@Inject
	private SubscriptionRepository repository;

	@POST
	@Consumes("application/json")
	@ApiOperation("Método responsável por salvar uma inscrição.")
	public Response create(Subscription entity) {
		em.persist(entity);
		return Response.created(
				UriBuilder.fromResource(SubscriptionRestResource.class).path(String.valueOf(entity.getId())).build())
				.build();
	}

	@GET
	@Path("payment/{paymentId}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Método responsável por bususer um membro pelo id da sessão da API de pagamento.", produces = MediaType.APPLICATION_JSON)
	public Subscription lookupByPaymentId(
			@ApiParam("Identifcador da sessão.") @PathParam("paymentId") String paymentId) {
		Subscription entity = repository.findByPaymentId(paymentId);
		if (entity == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		return entity;
	}

	@POST
	@Consumes("application/json")
	@Path("/checkout/{ticketId:[0-9][0-9]*}/{memberId:[0-9][0-9]*}")
	@ApiOperation("Método responsável por realizar o checkout de uma inscrição.")
	public Response checkout(@ApiParam("Identifcador do ticket.") @PathParam("ticketId") Long ticketId,
			@ApiParam("Identifcador do membro.") @PathParam("memberId") Long memberId) {
		
		Ticket ticket = em.find(Ticket.class, ticketId);
		if (ticket == null) {
			return Response.status(Status.NOT_FOUND).build();
		}

		Member member = em.find(Member.class, memberId);
		if (member == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		
		Subscription entity = null;

		List<Subscription> subscriptions = repository.findByMemberandTicket(member, ticket);
		
		if (subscriptions != null && !subscriptions.isEmpty()) {
			for (Subscription subscription : subscriptions) {
				entity = subscription;
			}
		}

		try {
			
			if (entity == null) 
				entity = new Subscription();

			entity.setDate(new Date());
			entity.setMember(member);
			entity.setStatus(0);
			entity.setTicket(ticket);

			subscriptionRegistration.register(entity);

		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).build();
		}

		return Response.status(Status.ACCEPTED)
				.entity(new CheckoutResponse().sessionId(entity.getPaymentId()).urlRedirect(entity.getUrlRedirect()))
				.build();
	}

	@DELETE
	@Path("/{id:[0-9][0-9]*}")
	@ApiOperation("Método responsável por excluir uma inscrição.")
	public Response deleteById(@ApiParam("Identifcador do inscrição.") @PathParam("id") Long id) {
		Subscription entity = em.find(Subscription.class, id);
		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		em.remove(entity);
		return Response.noContent().build();
	}

	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces("application/json")
	@ApiOperation("Método responsável por buscar uma inscrição pelo id.")
	public Response findById(@ApiParam("Identifcador do inscrição.") @PathParam("id") Long id) {
		TypedQuery<Subscription> findByIdQuery = em.createQuery(
				"SELECT DISTINCT s FROM Subscription s LEFT JOIN FETCH s.member LEFT JOIN FETCH s.ticket WHERE s.id = :entityId ORDER BY s.id",
				Subscription.class);
		findByIdQuery.setParameter("entityId", id);
		Subscription entity;
		try {
			entity = findByIdQuery.getSingleResult();
		} catch (NoResultException nre) {
			entity = null;
		}
		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok(entity).build();
	}

	@GET
	@Path("/member/{memberId:[0-9][0-9]*}")
	@Produces("application/json")
	@ApiOperation("Método responsável por listar as inscrições de um membro.")
	public List<Subscription> listByMember(@PathParam("memberId") Long memberId) {
		TypedQuery<Subscription> findAllQuery = em.createQuery(
				"SELECT DISTINCT s FROM Subscription s LEFT JOIN FETCH s.member LEFT JOIN FETCH s.ticket WHERE s.member.id = :memberId ORDER BY s.id",
				Subscription.class);
		if (em.find(Member.class, memberId) == null) {
			return null;
		}
		findAllQuery.setParameter("memberId", memberId);
		final List<Subscription> results = findAllQuery.getResultList();
		return results;
	}

	@PUT
	@Path("/{id:[0-9][0-9]*}")
	@Consumes("application/json")
	@ApiOperation("Método responsável por atualizar uma inscrição.")
	public Response update(@PathParam("id") Long id, Subscription entity) {
		if (entity == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if (id == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if (!id.equals(entity.getId())) {
			return Response.status(Status.CONFLICT).entity(entity).build();
		}
		if (em.find(Subscription.class, id) == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		try {
			entity = em.merge(entity);
		} catch (OptimisticLockException e) {
			return Response.status(Response.Status.CONFLICT).entity(e.getEntity()).build();
		}

		return Response.noContent().build();
	}

	class CheckoutResponse {

		String urlRedirect;
		String sessionId;

		public CheckoutResponse sessionId(String sessionId) {
			this.sessionId = sessionId;
			return this;
		}

		public CheckoutResponse urlRedirect(String urlRedirect) {
			this.urlRedirect = urlRedirect;
			return this;
		}

		public String getSessionId() {
			return sessionId;
		}

		public void setSessionId(String sessionId) {
			this.sessionId = sessionId;
		}

		public String getUrlRedirect() {
			return urlRedirect;
		}

		public void setUrlRedirect(String urlRedirect) {
			this.urlRedirect = urlRedirect;
		}

	}
}
