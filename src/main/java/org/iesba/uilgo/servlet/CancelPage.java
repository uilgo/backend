package org.iesba.uilgo.servlet;

import java.io.IOException;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.iesba.uilgo.data.SubscriptionRepository;
import org.iesba.uilgo.model.Subscription;
import org.iesba.uilgo.service.SubscriptionRegistration;

/**
 * Servlet implementation class CancelPage
 */
@WebServlet("/cancelpage")
public class CancelPage extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Inject
	private SubscriptionRepository repository;

	@Inject
	private Logger log;

	@Inject
	private SubscriptionRegistration registration;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CancelPage() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {

			log.info("Cancel Page ...");

			String paymentId = request.getParameter("session_id");

			if (paymentId != null && !"".equals(paymentId)) {

				log.info("Canceling payment id ".concat(paymentId));

				Subscription subscription = repository.findByPaymentId(paymentId);

				registration.cancel(subscription);

				response.getWriter().append("Payment Canceled! Subscription for ")
						.append(subscription.getMember().getName()).append(", and Ticket ")
						.append(subscription.getTicket().getDescription()).append(" with value ")
						.append(subscription.getTicket().getPrice().toString());
			} else {
				
				log.info("Payment id is not present!");
				
				response.getWriter().append("Payment id is not present!");
			}

		} catch (Exception e) {
			log.severe(e.getMessage());
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
