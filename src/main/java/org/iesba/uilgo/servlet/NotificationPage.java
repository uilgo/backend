package org.iesba.uilgo.servlet;

import java.io.IOException;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.iesba.uilgo.data.SubscriptionRepository;
import org.iesba.uilgo.model.Subscription;

import br.com.uol.pagseguro.api.PagSeguro;
import br.com.uol.pagseguro.api.PagSeguroEnv;
import br.com.uol.pagseguro.api.credential.Credential;
import br.com.uol.pagseguro.api.http.JSEHttpClient;
import br.com.uol.pagseguro.api.transaction.search.TransactionDetail;
import br.com.uol.pagseguro.api.utils.logging.SimpleLoggerFactory;

/**
 * Servlet implementation class SuccessPage
 */
@WebServlet("/notificationpage")
public class NotificationPage extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Inject
	private SubscriptionRepository repository;

	@Inject
	private EntityManager em;

	@Inject
	private Logger log;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NotificationPage() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String notificationCode = request.getParameter("notificationCode");

		if (notificationCode != null && !"".equals(notificationCode)) {

			final PagSeguro pagSeguro = PagSeguro.instance(new SimpleLoggerFactory(), new JSEHttpClient(),
					Credential.sellerCredential("deni.mascarenhas@gmail.com", "83100DC82CE64B92ACCA1F26EEE0F351"),
					PagSeguroEnv.PRODUCTION);

			TransactionDetail transaction = pagSeguro.transactions().search().byNotificationCode(notificationCode);

			Subscription subscription = null;

			if (transaction.getType().getTypeId() == 1 && transaction.getItems() != null
					&& !transaction.getItems().isEmpty()) {

				subscription = repository.findById(Long.valueOf(transaction.getItems().get(0).getId()));

				if (transaction.getStatus().getStatusId() == 3) {

					subscription.setStatus(1);

					em.persist(subscription);

					log.info(("Payment Success! Subscription for ").concat(subscription.getMember().getName())
							.concat(", and Ticket ").concat(subscription.getTicket().getDescription())
							.concat(" with value ").concat(subscription.getTicket().getPrice().toString()));

					response.getWriter().append("Payment Success! Subscription for ")
							.append(subscription.getMember().getName()).append(", and Ticket ")
							.append(subscription.getTicket().getDescription()).append(" with value ")
							.append(subscription.getTicket().getPrice().toString());

				} else if (transaction.getStatus().getStatusId() == 7) {

					subscription.setStatus(3);

					em.persist(subscription);

					log.info(("Payment Canceled! Subscription for ").concat(subscription.getMember().getName())
							.concat(", and Ticket ").concat(subscription.getTicket().getDescription())
							.concat(" with value ").concat(subscription.getTicket().getPrice().toString()));

					response.getWriter().append("Payment Canceled! Subscription for ")
							.append(subscription.getMember().getName()).append(", and Ticket ")
							.append(subscription.getTicket().getDescription()).append(" with value ")
							.append(subscription.getTicket().getPrice().toString());
				}
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
