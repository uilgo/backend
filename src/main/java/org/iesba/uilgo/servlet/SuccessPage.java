package org.iesba.uilgo.servlet;

import java.io.IOException;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.iesba.uilgo.data.SubscriptionRepository;
import org.iesba.uilgo.model.Subscription;
import org.iesba.uilgo.service.SubscriptionRegistration;

/**
 * Servlet implementation class SuccessPage
 */
@WebServlet("/successpage")
public class SuccessPage extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Inject
	private SubscriptionRepository repository;

	@Inject
	private Logger log;

	@Inject
	private SubscriptionRegistration registration;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SuccessPage() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		try {

			String paymentId = request.getParameter("session_id");

			if (paymentId != null && !"".equals(paymentId)) {

				Subscription subscription = repository.findByPaymentId(paymentId);

				registration.success(subscription);

				response.sendRedirect("http://front-uilgo.jelastic.saveincloud.net/pagamento-efetuado");

			} else {

				log.info("Payment id is not present!");

				response.getWriter().append("Payment id is not present!");
			}

		} catch (Exception e) {
			log.severe(e.getMessage());
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
