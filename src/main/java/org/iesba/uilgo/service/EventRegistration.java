package org.iesba.uilgo.service;

import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.iesba.uilgo.model.Event;

@Stateless
public class EventRegistration {

	@Inject
	private Logger log;

	@Inject
	private EntityManager em;

	public void register(Event event) throws Exception {
		log.info("Registering " + event.getName());
		em.persist(event);
	}
}
