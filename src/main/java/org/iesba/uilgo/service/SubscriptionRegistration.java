package org.iesba.uilgo.service;

import java.math.BigDecimal;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.iesba.uilgo.model.Subscription;
import org.iesba.uilgo.model.Ticket;
import org.iesba.uilgo.util.StripePaymentUtil;

@Stateless
public class SubscriptionRegistration {

	@Inject
	private Logger log;

	@Inject
	private EntityManager em;

	@Inject
	private StripePaymentUtil paymentUtil;
	// private PagseguroPaymentUtil paymentUtil;

	public Subscription register(Subscription subscription) throws Exception {
		log.info("Registering subscription for " + subscription.getTicket().getDescription());

		if (subscription.getTicket().getPrice().compareTo(BigDecimal.ZERO) > 0) {
			
			String[] checkoutParams = paymentUtil.checkout(subscription);
			
			subscription.setPaymentId(checkoutParams[0]);
			
			if (checkoutParams.length > 1) {
				subscription.setUrlRedirect(checkoutParams[1]);
			}
			subscription.setPaymentType(0);
			subscription.setStatus(0);
		} else {
			
			subscription.setUrlRedirect("http://front-uilgo.jelastic.saveincloud.net/pagamento-efetuado");
			
			subscription.setPaymentType(1);
			subscription.setStatus(1);
		}


		em.persist(subscription);

		Ticket ticket = em.find(Ticket.class, subscription.getTicket().getId());

		log.info("Locking ticket ... ".concat(subscription.getTicket().getDescription()));

		ticket.setCountNumber(ticket.getCountNumber() - 1);

		em.persist(ticket);

		return subscription;
	}

	public void cancel(Subscription subscription) throws Exception {

		subscription.setStatus(3);

		log.info("Canceling subscription for ".concat(subscription.getMember().getEmail()));
		log.info("Ticket data... ".concat(subscription.getTicket().getDescription()));
		log.info("Event data... ".concat(subscription.getTicket().getEvent().getName()));

		em.persist(subscription);

		Ticket ticket = em.find(Ticket.class, subscription.getTicket().getId());

		log.info("Releasing ticket ... ".concat(subscription.getTicket().getDescription()));

		ticket.setCountNumber(ticket.getCountNumber() + 1);

		em.persist(ticket);

		log.info(("Payment Canceled! Subscription for ").concat(subscription.getMember().getName())
				.concat(", and Ticket ").concat(subscription.getTicket().getDescription()).concat(" with value ")
				.concat(subscription.getTicket().getPrice().toString()));
	}

	public void success(Subscription subscription) throws Exception {

		subscription = em.find(Subscription.class, subscription.getId());

		subscription.setStatus(1);

		em.persist(subscription);

		log.info(("Payment Success! Subscription for ").concat(subscription.getMember().getName())
				.concat(", and Ticket ").concat(subscription.getTicket().getDescription()).concat(" with value ")
				.concat(subscription.getTicket().getPrice().toString()));
	}

}
