package org.iesba.uilgo.service;

import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.iesba.uilgo.model.Member;

@Stateless
public class MemberRegistration {

	@Inject
	private Logger log;

	@Inject
	private EntityManager em;

	public void register(Member member) throws Exception {
		log.info("Registering " + member.getName());
		em.persist(member);
	}
}
