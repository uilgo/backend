package org.iesba.uilgo.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotEmpty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@SuppressWarnings("serial")
@Entity
@XmlRootElement
@Table(name = "users", uniqueConstraints = @UniqueConstraint(columnNames = "login"))
@ApiModel(description = "Um usuário que possui credenciais de acesso ao sistema.")
public class User implements Serializable {

	@ApiModelProperty(value = "Identifiacdor único de um usuário.", required = true)
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ApiModelProperty(value = "Login de acesso ao sistema.", required = true)
	@NotNull
	@Size(min = 1, max = 200)
	@Pattern(regexp = "[^0-9]*", message = "Must not contain numbers")
	private String login;

	@ApiModelProperty(value = "Senha de acesso ao sistema.", required = true)
	@NotNull
	@NotEmpty
	private String password;
	
	@ApiModelProperty(value = "Regras de permissão de acesso ao sistema.", required = true)
	@NotNull
	@NotEmpty
	private String roles;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

}
