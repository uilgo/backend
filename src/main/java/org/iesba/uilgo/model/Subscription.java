package org.iesba.uilgo.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@SuppressWarnings("serial")
@Entity
@XmlRootElement
@ApiModel(description = "Representa uma inscrição de um determinado membro em um evento (através da compra de um ticket).")
@Table(name = "subscriptions")
public class Subscription implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(value = "Identificador único de uma inscrição.")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "member_id")
	@ApiModelProperty(value = "Referencia o membro inscrito.")
	private Member member;

	@ManyToOne
	@JoinColumn(name = "ticket_id")
	@ApiModelProperty(value = "Referencia o tipo de inscrição realizada. Os tickets são cadastrados de acordo useracterísticas específicas como: valor, tipo de acomodação, tipo de transporte e etc.")
	private Ticket ticket;

	@NotNull
	@ApiModelProperty(value = "Data em que a inscrição foi efetuada.")
	private Date date;

	@NotNull
	@ApiModelProperty(value = "Indica a situação da inscrição. 0 Pendente, 1 Ativa, 2 Inativa e 3 Cancelada")
	private Integer status;
	
	@ApiModelProperty(value = "Indica o ID do pagamento feito via integração com API de billing.")
	@Column(name = "payment_id")
	private String paymentId;
	
	@ApiModelProperty(value = "Indica a url de integração para finalização do checkout via integração com API de billing.")
	@Column(name = "url_redirect")
	private String urlRedirect;
	
	@ApiModelProperty(value = "Indica o tipo de pagamento inscrição. 0 Normal e 1 Cortesia ")
	@Column(name = "payment_type")
	private Integer paymentType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getUrlRedirect() {
		return urlRedirect;
	}

	public void setUrlRedirect(String urlRedirect) {
		this.urlRedirect = urlRedirect;
	}

	public Integer getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(Integer paymentType) {
		this.paymentType = paymentType;
	}

}
