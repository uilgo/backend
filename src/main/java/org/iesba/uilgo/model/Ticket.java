package org.iesba.uilgo.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotEmpty;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;

@SuppressWarnings("serial")
@Entity
@XmlRootElement
@Api("Tickets são cadastrados no evento para que o evento tenha a capacidade de aceitar tipos de inscrições diferentes. Cada Ticket possui seu preço, a quantidade disponível e sua descrição. Por ex.: Acomodação de casal / 10 disponíveis / R$ 120,00.")
@Table(name = "tickets")
public class Ticket implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(value = "Identificador único de um ticket.")
	private Long id;

	@NotNull
	@Digits(fraction = 0, integer = 12)
	@Column(name = "count_number")
	@ApiModelProperty(value = "Quantidade de tickets disponíveis.")
	private Integer countNumber;

	@NotNull
	@ApiModelProperty(value = "Valor de um ticket.")
	private BigDecimal price;

	@NotNull
	@NotEmpty
	@ApiModelProperty(value = "Descrição textual sobre o ticket.")
	private String description;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "event_id")
	@ApiModelProperty(value = "Referencia o evento a qual o ticket pertence.")
	private Event event;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCountNumber() {
		return countNumber;
	}

	public void setCountNumber(Integer countNumber) {
		this.countNumber = countNumber;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

}
