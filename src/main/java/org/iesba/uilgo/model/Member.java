/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.iesba.uilgo.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@SuppressWarnings("serial")
@Entity
@XmlRootElement
@Table(name = "members", uniqueConstraints = @UniqueConstraint(columnNames = "email"))
@ApiModel(description = "Um membro cadastrado que pode realizar inscrições em eventos.")
public class Member implements Serializable {

	@ApiModelProperty(value = "Identificador único de um membro.")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ApiModelProperty(value = "Nome do membro.")
	@NotNull
	@Size(min = 1, max = 200)
	private String name;

	@ApiModelProperty(value = "Email do membro. Deve ser informado um email válido.")
	@NotNull
	@NotEmpty
	@Email
	private String email;

	@ApiModelProperty(value = "Telefone do membro.")
	@NotNull
	@Column(name = "phone_number")
	private String phoneNumber;

	@ApiModelProperty(value = "Usuário relacionado ao membro. O membro que possui usuário poderá ter credenciais de acesso ao sistema.")
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	@ApiModelProperty(value = "Número de identificação pessoal. Tem um valor e um formato de acordo com o tipo.")
	@Column(name = "identity_number")
	private String identityNumber;

	@ApiModelProperty(value = "Tipo de identificação pessoal. Dependendo do país poderá ser CNH, RG, CPF e etc...")
	@Column(name = "identity_type")
	private String identityType;

	@ApiModelProperty(value = "Sexo do membro.")
	private String sex;

	@ApiModelProperty(value = "Data de nascimento do membro.")
	@Temporal(TemporalType.DATE)
	private Date birth;

	@ApiModelProperty(value = "Endereço de residência do membro.")
	private String address;

	@ApiModelProperty(value = "Nome do pai.")
	@Column(name = "fathers_name")
	private String fathersName;

	@ApiModelProperty(value = "Nome da mãe.")
	@Column(name = "mothers_name")
	private String mothersName;

	@ApiModelProperty(value = "Telefone de contato do pai.")
	@Column(name = "fathers_phone")
	private String fathersPhone;

	@ApiModelProperty(value = "Telefone de contato da mãe.")
	@Column(name = "mothers_phone")
	private String mothersPhone;

	@ApiModelProperty(value = "Nome do pastor.")
	@Column(name = "pastors_name")
	private String pastorsName;

	@ApiModelProperty(value = "Telefone de contato do pastor.")
	@Column(name = "pastors_phone")
	private String pastorsPhone;
	
	@ApiModelProperty(value = "Cidade do membro.")
	private String city;	
	
	@ApiModelProperty(value = "Estado do membro.")
	private String state;
	
	@ApiModelProperty(value = "Distrito ou bairo do membro.")
	private String district;

	public Member() {

	}

	public Member(Long id, Date birth) {
		this.id = id;
		this.birth = birth;
	}

	public Member(Long id, String email, String identityNumber) {
		this.id = id;
		this.email = email;
		this.identityNumber = identityNumber;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getIdentityNumber() {
		return identityNumber;
	}

	public void setIdentityNumber(String identityNumber) {
		this.identityNumber = identityNumber;
	}

	public String getIdentityType() {
		return identityType;
	}

	public void setIdentityType(String identityType) {
		this.identityType = identityType;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Date getBirth() {
		return birth;
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getFathersName() {
		return fathersName;
	}

	public void setFathersName(String fathersName) {
		this.fathersName = fathersName;
	}

	public String getMothersName() {
		return mothersName;
	}

	public void setMothersName(String mothersName) {
		this.mothersName = mothersName;
	}

	public String getFathersPhone() {
		return fathersPhone;
	}

	public void setFathersPhone(String fathersPhone) {
		this.fathersPhone = fathersPhone;
	}

	public String getMothersPhone() {
		return mothersPhone;
	}

	public void setMothersPhone(String mothersPhone) {
		this.mothersPhone = mothersPhone;
	}

	public String getPastorsName() {
		return pastorsName;
	}

	public void setPastorsName(String pastorsName) {
		this.pastorsName = pastorsName;
	}

	public String getPastorsPhone() {
		return pastorsPhone;
	}

	public void setPastorsPhone(String pastorsPhone) {
		this.pastorsPhone = pastorsPhone;
	}

}
