--
-- JBoss, Home of Professional Open Source
-- Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
-- contributors by the @authors tag. See the copyright.txt in the
-- distribution for a full listing of individual contributors.
--
-- Licensed under the Apache License, Version 2.0 (the "License")
-- you may not use this file except in compliance with the License.
-- You may obtain a copy of the License at
-- http://www.apache.org/licenses/LICENSE-2.0
-- Unless required by applicable law or agreed to in writing, software
-- distributed under the License is distributed on an "AS IS" BASIS,
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
-- See the License for the specific language governing permissions and
-- limitations under the License.
--

-- You can use this file to load seed data into the database using SQL statements
insert into Users (id, login, password, roles) values (1,'uilgo.adm@gmail.com','YWRtaW4=', 'admin');
--insert into Users (id, login, password) values (2,'user2','1234');
--insert into Users (id, login, password) values (3,'user3','1234');
--insert into Users (id, login, password) values (4,'user4','1234');
--insert into Users (id, login, password) values (5,'user5','1234');
--insert into Users (id, login, password) values (6,'user6','1234');
--insert into Users (id, login, password) values (7,'user7','1234');
--insert into Users (id, login, password) values (8,'user8','1234');
--insert into Users (id, login, password) values (9,'user9','1234');
--insert into Users (id, login, password) values (10,'user10','1234');

insert into Members (id, name, email, phone_number, identity_number, birth, identity_type, sex, fathers_name, mothers_name, fathers_phone, mothers_phone, pastors_name, pastors_phone, address) values (1, 'Joao Santana', 'jao.santna@gmail.com', '212', '123', DATE '1980-07-11','CPF','M','Pai','Mae','123','123','Pastor','123','endereco xxx')
insert into Members (id, name, email, phone_number, identity_number, birth, identity_type, sex, fathers_name, mothers_name, fathers_phone, mothers_phone, pastors_name, pastors_phone, address) values (2, 'Jose Silva', 'jose@gmail.com', '11', '321', DATE '1991-02-03','CPF','M','Pai','Mae','123','123','Pastor','123','endereco xxx')
insert into Members (id, name, email, phone_number, identity_number, birth, identity_type, sex, fathers_name, mothers_name, fathers_phone, mothers_phone, pastors_name, pastors_phone, address) values (3, 'Luiz Cardoso', 'luiz@gmail.com', '22', '234', DATE '1983-05-06','CPF','M','Pai','Mae','123','123','Pastor','123','endereco xxx')
insert into Members (id, name, email, phone_number, identity_number, birth, identity_type, sex, fathers_name, mothers_name, fathers_phone, mothers_phone, pastors_name, pastors_phone, address) values (4, 'Maria Goncalvez', 'maria@gmail.com', '33', '423', DATE '1987-06-21','CPF','F','Pai','Mae','123','123','Pastor','123','endereco xxx')
insert into Members (id, name, email, phone_number, identity_number, birth, identity_type, sex, fathers_name, mothers_name, fathers_phone, mothers_phone, pastors_name, pastors_phone, address) values (5, 'Carla Souza', 'carla@gmail.com', '44', '345', DATE '1984-02-22','CPF','F','Pai','Mae','123','123','Pastor','123','endereco xxx')
insert into Members (id, name, email, phone_number, identity_number, birth, identity_type, sex, fathers_name, mothers_name, fathers_phone, mothers_phone, pastors_name, pastors_phone, address) values (6, 'Joana Teles', 'joana@gmail.com', '55', '543', DATE '1985-03-18','CPF','F','Pai','Mae','123','123','Pastor','123','endereco xxx')
insert into Members (id, name, email, phone_number, identity_number, birth, identity_type, sex, fathers_name, mothers_name, fathers_phone, mothers_phone, pastors_name, pastors_phone, address) values (7, 'Cristina Teles', 'cirstina@gmail.com', '66', '456', DATE '1992-03-02','CPF','F','Pai','Mae','123','123','Pastor','123','endereco xxx')
insert into Members (id, name, email, phone_number, identity_number, birth, identity_type, sex, fathers_name, mothers_name, fathers_phone, mothers_phone, pastors_name, pastors_phone, address) values (8, 'Arnaldo Filho', 'arnaldo@gmail.com', '77', '654', DATE '1990-09-13','CPF','M','Pai','Mae','123','123','Pastor','123','endereco xxx')
insert into Members (id, name, email, phone_number, identity_number, birth, identity_type, sex, fathers_name, mothers_name, fathers_phone, mothers_phone, pastors_name, pastors_phone, address) values (9, 'Ana Julia', 'ana@gmail.com', '88', '567', DATE '1987-10-16','CPF','F','Pai','Mae','123','123','Pastor','123','endereco xxx')
insert into Members (id, name, email, phone_number, identity_number, birth, identity_type, sex, fathers_name, mothers_name, fathers_phone, mothers_phone, pastors_name, pastors_phone, address) values (10, 'Roberto Carlos', 'roberto@gmail.com', '99', '765', DATE '1984-05-29','CPF','M','Pai','Mae','123','123','Pastor','123','endereco xxx')

insert into Events (id, name, start_date, stop_date) values (1, 'Retiro de Solteiros 2019', DATE '2019-07-15', DATE '2019-07-17')
insert into Events (id, name, start_date, stop_date) values (2, 'Retiro de Casados 2019', DATE '2019-09-01', DATE '2019-09-02')
insert into Events (id, name, start_date, stop_date) values (3, 'Retiro de Líderes 2019', DATE '2019-07-04', DATE '2019-07-06')
insert into Events (id, name, start_date, stop_date) values (4, 'Retiro de Discipuladores 2019', DATE '2019-11-01', DATE '2019-11-03')
insert into Events (id, name, start_date, stop_date) values (5, 'Retiro de Mulheres 2019', DATE '2019-07-07', DATE '2019-07-08')

insert into Tickets (id, count_number, price, description, event_id) values (1, 5, 120.0, 'Acomodacao em dormitorio', 1)
insert into Tickets (id, count_number, price, description, event_id) values (2, 4, 180.0, 'Acomodacao em apartamento', 1)
insert into Tickets (id, count_number, price, description, event_id) values (3, 5, 120.0, 'Acomodacao em dormitorio', 2)
insert into Tickets (id, count_number, price, description, event_id) values (4, 4, 180.0, 'Acomodacao em apartamento', 2)
insert into Tickets (id, count_number, price, description, event_id) values (5, 5, 120.0, 'Acomodacao em dormitorio', 3)
insert into Tickets (id, count_number, price, description, event_id) values (6, 4, 180.0, 'Acomodacao em apartamento', 3)
insert into Tickets (id, count_number, price, description, event_id) values (7, 5, 120.0, 'Acomodacao em dormitorio', 4)
insert into Tickets (id, count_number, price, description, event_id) values (8, 4, 180.0, 'Acomodacao em apartamento', 4)
insert into Tickets (id, count_number, price, description, event_id) values (9, 5, 120.0, 'Acomodacao em dormitorio', 5)
insert into Tickets (id, count_number, price, description, event_id) values (10, 4, 180.0, 'Acomodacao em apartamento', 5)

insert into Events (id, name, start_date, stop_date) values (6, 'Retiro de Solteiros 2021 - ACUIPE 2021', DATE '2021-06-15', DATE '2021-06-17')
insert into Tickets (id, count_number, price, description, event_id) values (11, 100, 800, 'Jovens e adultos', 6)
insert into Tickets (id, count_number, price, description, event_id) values (12, 100, 0, 'Criança < 4 anos', 6)
insert into Tickets (id, count_number, price, description, event_id) values (13, 100, 470, 'Crianças 5 a 9 anos', 6)
insert into Tickets (id, count_number, price, description, event_id) values (14, 100, 630, 'Crianças 10 a 13 anos', 6)
insert into Tickets (id, count_number, price, description, event_id) values (15, 100, 1040, 'Jovens e adultos + Onibus (R$ 240,00)', 6)
insert into Tickets (id, count_number, price, description, event_id) values (16, 100, 240, 'Criança < 4 anos + Onibus (R$ 240,00)', 6)
insert into Tickets (id, count_number, price, description, event_id) values (17, 100, 710, 'Crianças 5 a 9 anos + Onibus (R$ 240,00)', 6)
insert into Tickets (id, count_number, price, description, event_id) values (18, 100, 870, 'Crianças 10 a 13 anos + Onibus (R$ 240,00)', 6)
insert into Tickets (id, count_number, price, description, event_id) values (19, 100, 120, 'Diária avulsa', 6)

insert into Subscriptions (id, member_id, ticket_id, date, status, payment_id) values (1, 1, 11, DATE '2019-03-10', 0, 'XX')
insert into Subscriptions (id, member_id, ticket_id, date, status, payment_id) values (2, 2, 11, DATE '2019-03-10', 0, 'XX')
insert into Subscriptions (id, member_id, ticket_id, date, status, payment_id) values (3, 3, 11, DATE '2019-03-10', 0, 'XX')
insert into Subscriptions (id, member_id, ticket_id, date, status, payment_id) values (4, 4, 11, DATE '2019-03-10', 0, 'XX')
insert into Subscriptions (id, member_id, ticket_id, date, status, payment_id) values (5, 5, 11, DATE '2019-03-10', 0, 'XX')
insert into Subscriptions (id, member_id, ticket_id, date, status, payment_id) values (6, 6, 11, DATE '2019-03-10', 0, 'XX')
insert into Subscriptions (id, member_id, ticket_id, date, status, payment_id) values (7, 7, 11, DATE '2019-03-10', 0, 'XX')
insert into Subscriptions (id, member_id, ticket_id, date, status, payment_id) values (8, 8, 11, DATE '2019-03-10', 0, 'XX')
insert into Subscriptions (id, member_id, ticket_id, date, status, payment_id) values (9, 9, 11, DATE '2019-03-10', 0, 'XX')
insert into Subscriptions (id, member_id, ticket_id, date, status, payment_id) values (10, 10, 11, DATE '2019-03-10', 0, 'XX')
insert into Subscriptions (id, member_id, ticket_id, date, status, payment_id) values (11, 1, 1, DATE '2019-03-10', 0, 'XX')
insert into Subscriptions (id, member_id, ticket_id, date, status, payment_id) values (12, 2, 1, DATE '2019-03-10', 0, 'XX')
insert into Subscriptions (id, member_id, ticket_id, date, status, payment_id) values (13, 3, 1, DATE '2019-03-10', 0, 'XX')
insert into Subscriptions (id, member_id, ticket_id, date, status, payment_id) values (14, 4, 1, DATE '2019-03-10', 0, 'XX')
insert into Subscriptions (id, member_id, ticket_id, date, status, payment_id) values (15, 5, 1, DATE '2019-03-10', 0, 'XX')
insert into Subscriptions (id, member_id, ticket_id, date, status, payment_id) values (16, 6, 1, DATE '2019-03-10', 0, 'XX')
insert into Subscriptions (id, member_id, ticket_id, date, status, payment_id) values (17, 7, 1, DATE '2019-03-10', 0, 'XX')
insert into Subscriptions (id, member_id, ticket_id, date, status, payment_id) values (18, 8, 1, DATE '2019-03-10', 0, 'XX')
insert into Subscriptions (id, member_id, ticket_id, date, status, payment_id) values (19, 9, 1, DATE '2019-03-10', 0, 'XX')
insert into Subscriptions (id, member_id, ticket_id, date, status, payment_id) values (20, 10, 1, DATE '2019-03-10', 0, 'XX')
insert into Subscriptions (id, member_id, ticket_id, date, status, payment_id) values (21, 1, 2, DATE '2019-03-10', 0, 'XX')
insert into Subscriptions (id, member_id, ticket_id, date, status, payment_id) values (22, 2, 2, DATE '2019-03-10', 0, 'XX')

