FROM jboss/wildfly:16.0.0.Final
MAINTAINER Formworks

COPY ./docker/standalone.conf ${WILDFLY_HOME}/bin/

COPY ./target/backend.war ${DEPLOYMENT_DIR}

ADD docker/customization /opt/jboss/wildfly/customization/

CMD ["/opt/jboss/wildfly/customization/config.sh"]

